function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function randomFontSize(elements)
{
    console.log(elements);
    for (let i = 0; i < elements.length; i++){
        elements[i].style.fontSize = (getRandomInt(20,31) + "px");
    }
}

function randomColors(elements, colors)
{
    for (let i = 0; i < elements.length; i++){
        elements[i].style.color = colors[getRandomInt(0, colors.length + 1)];
    }
}

window.onload = function (){
    let colors = ['green', 'red', 'orange', 'black', 'green']
    let elements = document.getElementsByClassName("popular-tags__popular-tag");
    randomFontSize(elements);
    randomColors(elements, colors);
}